FROM node:20-alpine

ADD package.json package-lock.json tsconfig.json nest-cli.json .prettierrc ./
ADD src/ ./src/

RUN npm install

CMD ["npm", "start"]
